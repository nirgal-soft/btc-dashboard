## btc-dashboard
A small rust cli for viewing [mempool.space](htts://mempool.space) info right in your terminal.

## Motivation
This is a cli 'mempool.space'-lite copy. It is simply a view into the
mempool.space interface without the need for a web browser.

## Screenshots

## Tech/Frameworks
**Built with**
- Rust
- tui-rs
- [mempool.space](https://mempool.space) api
- [bitaps](https://bitaps.com) api

## Installation
Run `cargo install --path .`

## Tests
Run `cargo test`

## Credits
Thanks to [mempool.space](https://mempool.space) and [bitps](https://bitaps.com) for their awesome apis.
