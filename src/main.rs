use std::io;
use std::io::{Stdout, Write, stdout, stdin};
use std::error::Error;
use termion::event::Key;
use tui::Terminal;
use tui::backend::TermionBackend;
use termion::raw::IntoRawMode;
use tui::widgets::{Widget, Block, Borders, BorderType};
use tui::layout::{Layout, Constraint, Direction, Rect};
use tui::style::{Color, Modifier, Style};
use tui::text::Span;

mod renderer;
mod input;

use renderer::Renderer;

fn main() -> Result<(), Box<dyn Error>>{
  let events = input::Events::with_config(input::Config{..input::Config::default()});
  let stdout = io::stdout().into_raw_mode()?;
  let backend = TermionBackend::new(stdout);
  let mut terminal = Terminal::new(backend)?;

  let m_block = MinedBlock{
    avg_sat_vbyte: 111,
    sat_vbyte_low: 107,
    sat_vbyte_high: 623,
    block_size: 1.46,
    num_tx: 1146,
    time_mined: Duration::from_secs(60),
    block_height: 684223,
  };

  terminal.clear();
  loop{
    match events.next()?{
      input::Event::Input(key) => match key{
        Key::Char('q') => break,
        _ => ()
      },
      input::Event::Tick => {
        terminal.draw(|f|{
          let size = f.size();
          let block = Block::default()
            .title("Block")
            .border_type(BorderType::Rounded)
            .borders(Borders::ALL);
          f.render_widget(block, size);
          let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(4)
            .constraints([Constraint::Percentage(10), Constraint::Percentage(10)].as_ref())
            .split(f.size());

          let top_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(10), Constraint::Percentage(10)].as_ref())
            .split(chunks[0]);
          let block = Block::default()
            .title(vec![
              Span::styled("With", Style::default().fg(Color::White)),
              Span::from(" background"),
            ])
            .style(Style::default().bg(Color::LightYellow));
          f.render_widget(m_block.get_block(), top_chunks[0]);
        })?;
      }
    }
  }
  
  Ok(())
}

use std::time::Duration;
#[derive(Clone, Copy)]
pub struct MinedBlock{
  avg_sat_vbyte: i32,
  sat_vbyte_low: i32,
  sat_vbyte_high: i32,
  num_tx: i32,
  block_height: i32,
  block_size: f32,
  time_mined: Duration,
}

impl MinedBlock{
  fn get_block(&self) -> Block<'static>{
    let title = format!("{}", self.block_height);
    let block = Block::default()
      .title(title)
      .style(Style::default().bg(Color::LightBlue));
    block
  }
}
