use std::io;
use std::io::{Stdout, Write, stdout, stdin};
use tui::Terminal;
use tui::backend::TermionBackend;
use termion::raw::IntoRawMode;
use termion::style;
use termion::color;

pub struct Renderer{
  stdout: termion::raw::RawTerminal<Stdout>,
  width: u16,
  height: u16,
}

impl Renderer{
  pub fn new() -> Renderer{
    Renderer{
      stdout: io::stdout().into_raw_mode().unwrap(),
      width: 0,
      height: 0,
    }
  }

  pub fn init(&mut self){
    let termsize = termion::terminal_size().ok();
    self.width = termsize.map(|(w,_)| w - 2).unwrap();
    self.height = termsize.map(|(_,h)| h - 2).unwrap();
    write!(self.stdout, "{}{}{}",
           termion::clear::All,
           termion::cursor::Goto(self.width, self.height),
           termion::cursor::Hide).unwrap();
    self.stdout.flush().unwrap();
  }

  pub fn draw(&mut self, text: String){
    let st = style::Bold;
    let col = color::Fg(color::Green);
    write!(
      self.stdout, "{}{}", termion::cursor::Goto(self.width/2, self.height/2),
      termion::clear::CurrentLine).unwrap();
    
    self.stdout.flush().unwrap();
  }

  pub fn shutdown(&mut self){
    write!(self.stdout, "{}", termion::cursor::Show).unwrap();
  }
}
