use std::sync::mpsc;
use termion::event::Key;
use std::time::{Duration, Instant};
use std::sync::{
  atomic::{AtomicBool, Ordering},
  Arc,
};
use std::thread;
use std::io;
use std::io::{Stdout, Write, stdout, stdin};
use termion::raw::IntoRawMode;
use termion::input::TermRead;

pub enum Event<I>{
  Input(I),
  Tick,
}

//a small event handler that wraps termion input and tick events
pub struct Events{
  rx: mpsc::Receiver<Event<Key>>, //creates a receiver with type T, can only be owned by 1 channel
  input_handle: thread::JoinHandle<()>, //detaches assocaited thread when it is dropped
  ignore_exit_key: Arc<AtomicBool>, //reference counter wrapping an atomic bool
  tick_handle: thread::JoinHandle<()>, //another join handle
}

#[derive(Debug, Clone, Copy)]
pub struct Config{ //a struct for input configuration
  pub exit_key: Key,
  pub tick_rate: Duration,
}

impl Default for Config{ //default impl for Config
  fn default() -> Config {
    return Config{
      exit_key: Key::Char('q'),
      tick_rate: Duration::from_millis(250),
    };
  }
}

impl Events{
  pub fn new() -> Events{
    return Events::with_config(Config::default()); //returns an Events strut with default config
  } 

  pub fn with_config(config: Config) -> Events{
    let (tx, rx) = mpsc::channel(); //create a new channel, with sender/receiver
    let ignore_exit_key = Arc::new(AtomicBool::new(false)); //new Arc atom bool
    let input_handle = { //new input_handler
      let tx = tx.clone(); //clones the sender
      let ignore_exit_key = ignore_exit_key.clone(); //clones the arc atom bool
      thread::spawn(move || { //spawns a new thread to handle the quit key
        let stdin = io::stdin(); //get a new handle to the input buffer
        for evt in stdin.keys(){ //returns an iterator for key inputs
          if let Ok(key) = evt{ //if the result of evt is Ok
            if let Err(err) = tx.send(Event::Input(key)){ //if the result of tx is Err
              eprintln!("{}", err); //handle the error
              return; //return from the function
            }
            //if not ignoring the exit key, and the exit key was pressed
            if !ignore_exit_key.load(Ordering::Relaxed) && key == config.exit_key{
              eprintln!("the quit button was pressed");
              //return;
            }
          }
        }
      })
    };
    
    let tick_handle = {
      thread::spawn(move || loop {
        if tx.send(Event::Tick).is_err(){
          break;
        }
        thread::sleep(config.tick_rate);
      })
    };
    return Events{
      rx,
      ignore_exit_key,
      input_handle,
      tick_handle,
    }
  }

  pub fn next(&self) -> Result<Event<Key>, mpsc::RecvError>{
    return self.rx.recv();
  }

  pub fn disable_exit_key(&mut self){
    self.ignore_exit_key.store(true, Ordering::Relaxed);
  }

  pub fn enable_exit_key(&mut self){
    self.ignore_exit_key.store(false, Ordering::Relaxed);
  }
}

